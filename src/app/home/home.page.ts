import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  err_msg : string;
  err_hidden : boolean;
  weight : number;
  genders = [];
  gender : string;
  times = [];
  time : string;
  bottles = [];
  bottle : string;
  promilles: number;
  constructor() {}

  ngOnInit() {
    this.err_hidden = true;
    this.genders.push("Male");
    this.genders.push("Female");
    for (let i = 0; i <= 24; i++) {
      this.times.push(String(i));  
    }
    for (let i = 1; i <= 10; i++) {
      this.bottles.push(String(i));
    }
    this.gender = "Male";
    this.time = "1";
    this.bottle = "1";
  }

  calculate(){
    this.err_hidden = true;
    if (this.weight == NaN || this.weight <= 0 || this.weight == undefined ){
      this.err_msg = "You didn't enter weight";
      this.err_hidden = false;
      console.log("No weight");
    }
    else {
      console.log(this.weight);
      
      const litres = parseInt(this.bottle) * 0.33;
      let grams = litres * 8 * 4.5;
      const burning = this.weight / 10;
      grams = grams - burning * parseInt(this.time);
      switch (this.gender) {
        case "Male":
          const resultM = grams / (this.weight * 0.7);
          this.promilles = resultM >=0 ? resultM : 0;
          break;
        case "Female":
          const resultF = grams / (this.weight * 0.6);
          this.promilles = resultF >=0 ? resultF : 0;
          break;
        default:
          this.promilles = -1 //It's wrong number for debug
          break;
      }
    }
  }
}
